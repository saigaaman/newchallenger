﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class EnemyDamageText : MonoBehaviour
{
    #pragma warning disable 0649
    #pragma warning disable 0414


    public Text damageText;
    //　フェードアウトするスピード
    private float fadeOutSpeed = 1f;
    //　移動値
    [SerializeField]
    private float moveSpeed = 2.0f;




    void Awake()
    {
        damageText = GetComponentInChildren<Text>();
    }

    void LateUpdate()
    {
        transform.rotation = Camera.main.transform.rotation;
        transform.position += Vector3.up * moveSpeed * Time.deltaTime;

        //Color.Lerpを使って現在のテキストのカラーのアルファを1から0へとfadeOutSpeedの速さで変化
        damageText.color = Color.Lerp(damageText.color, new Color(255f, 255f, 255f, 0f), fadeOutSpeed * Time.deltaTime);

        if (damageText.color.a <= 0.1f)
        {
            Destroy(gameObject);
        }
    }





}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UiMover : MonoBehaviour
{
#pragma warning disable 0649
    //対象のキャンバス
    [SerializeField]
    private Canvas canvas;
    //対象のカメラ
    [SerializeField]
    private Transform targetTfm;

    //uGUIコンポーネントは通常のTransformではなくて、RectTransformを使う必要があります。
    //豆知識としてtransformのサブクラスでもあるのでコンポーネントを取得する際は
    //var rectTransform1 = GetComponent<RectTransform>();
    //var rectTransform2 = transform as RectTransform;
    //以上どちらでもつかえる
    //このRectTransformを設定することで、UI要素の位置・サイズを変更することができます。
    private RectTransform canvasRectTfm;
    private RectTransform myRectTfm;
    private Vector3 offset = new Vector3(0, 1.5f, 0);

    void Start()
    {
        canvasRectTfm = canvas.GetComponent<RectTransform>();
        myRectTfm = GetComponent<RectTransform>();
    }

    void Update()
    {
        Vector2 pos;

        switch (canvas.renderMode)
        {
            //レンダーモードがスクリーン依存の場合はこの処理
            case RenderMode.ScreenSpaceOverlay:
                myRectTfm.position = RectTransformUtility.WorldToScreenPoint(Camera.main, targetTfm.position + offset);

                break;
            //レンダーモードがカメラ依存の場合はこの処理
            case RenderMode.ScreenSpaceCamera:
                Vector2 screenPos = RectTransformUtility.WorldToScreenPoint(Camera.main, targetTfm.position + offset);
                RectTransformUtility.ScreenPointToLocalPointInRectangle(canvasRectTfm, screenPos, Camera.main, out pos);
                myRectTfm.localPosition = pos;
                break;
            //レンダーモードがワールド依存の場合はこの処理
            case RenderMode.WorldSpace:
                //カメラを見続ける処理
                myRectTfm.LookAt(Camera.main.transform);

                break;
        }
    }
}

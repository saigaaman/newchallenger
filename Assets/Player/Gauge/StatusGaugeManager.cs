﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatusGaugeManager : MonoBehaviour
{
    public GameObject _target;
    private PlayerStatus _playerStatus;
    public Image _HpGauge;
    public Image _ApGauge;
    public Image _MpGauge;



    // Start is called before the first frame update
    void Start()
    {
        _playerStatus = _target.GetComponent<PlayerStatus>();   
    }



    //以下３ステータスを外部からアクセス可能
    //最大ステータス/現在ステータス*100でゲージを出す
    public void HpGaugeUpdate(float maxHp ,float hp)
    {
        _HpGauge.fillAmount = (hp/maxHp);
    }

    public void ApGaugeUpdate(float maxAp,float ap)
    {
        _ApGauge.fillAmount = (ap/maxAp);
    }

    public void MpGaugeUpdate(float maxMp,float mp)
    {
        _MpGauge.fillAmount = (mp/maxMp);
    }



}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    #pragma warning disable 0649
    #pragma warning disable 0414
    //　DamageUIプレハブ
    [SerializeField]
    private GameObject damageUI;
    [SerializeField]
    private GameObject _playerSoundObject;
    PlayerSoundsScript _playerSoundsScript;

    public Collider _superSlash;
    public Collider _speedySlash;
    public Collider _upSlash;
    public Collider _Reverse;
    public Collider _strongSlash;
    public Collider _backSlash;
    public Collider _fullSwing;

    Animator _animator;
    private AttackAnim _attackAnim;
    private Attack1 _attack1;
    private Attack2 _attack2;
    private Attack3 _attack3;
    private Attack4 _attack4;
    private Attack5 _attack5;
    private Attack6 _attack6;
    private Attack7 _attack7;

    AnimatorClipInfo[] clipInfo;
    string clipName;


    //ダメージを与える為の双方のステータス
    PlayerStatus _playerStatus;
    EnemyStatus _enemyStatus;
    PlayerController _playerController;

    //攻撃倍率
    private float _attackValue;


    private void Start()
    {
        //現在のアニメーションの情報取得
        //アニメーションを取得
        _animator = GetComponent<Animator>();
        _attackAnim = _animator.GetBehaviour<AttackAnim>();
        _attack1 = _animator.GetBehaviour<Attack1>();
        _attack2 = _animator.GetBehaviour<Attack2>();
        _attack3 = _animator.GetBehaviour<Attack3>();
        _attack4 = _animator.GetBehaviour<Attack4>();
        _attack5 = _animator.GetBehaviour<Attack5>();
        _attack6 = _animator.GetBehaviour<Attack6>();
        _attack7 = _animator.GetBehaviour<Attack7>();
        //プレイヤーのステータスを取得
        _playerStatus = GetComponent<PlayerStatus>();
        _playerController = GetComponent<PlayerController>();
        //サウンドをセット
        _playerSoundsScript = _playerSoundObject.GetComponent<PlayerSoundsScript>();



    }


    public void HitOn()
    {
        //現在のアニメーションの情報取得
        clipInfo = _animator.GetCurrentAnimatorClipInfo(0);
        //現在のアニメーションの名前を取得
        clipName = clipInfo[0].clip.name;

        //該当の名前のコライダーをオンにする
        switch (clipName)
        {
            case "SuperSlash":
                _attackValue = 1.0f;
                _playerStatus.DownAp(8);
                AttackVoiceAndSoundSelecter();
                _superSlash.enabled = true;
                break;
            case "SpeedySlash":
                _attackValue = 1.1f;
                _playerStatus.DownAp(8);
                AttackVoiceAndSoundSelecter();
                _speedySlash.enabled = true;
                break;
            case "UpSlash":
                _attackValue = 1.2f;
                _playerStatus.DownAp(10);
                AttackVoiceAndSoundSelecter();
                _upSlash.enabled = true;
                break;
            case "Reverse":
                _attackValue = 1.2f;
                _playerStatus.DownAp(10);
                AttackVoiceAndSoundSelecter();
                _Reverse.enabled = true;
                break;
            case "StrongSlash":
                _attackValue = 1.5f;
                _playerStatus.DownAp(12);
                AttackVoiceAndSoundSelecter();
                _strongSlash.enabled = true;
                break;
            case "BackSlash":
                _attackValue = 1.45f;
                _playerStatus.DownAp(12);
                AttackVoiceAndSoundSelecter();
                _backSlash.enabled = true;
                break;
            case "FullSwing":
                _attackValue = 1.7f;
                _playerStatus.DownAp(15);
                AttackVoiceAndSoundSelecter();
                _fullSwing.enabled = true;
                break;
            default:
                Debug.Log("どれにも該当しませんでした");
                break;
        }
    }


    public void HitOff()
    {

        //該当の名前のコライダーをオンにする
        switch (clipName)
        {
            case "SuperSlash":
                _superSlash.enabled = false;
                break;
            case "SpeedySlash":
                _speedySlash.enabled = false;
                break;
            case "UpSlash":
                _upSlash.enabled = false;
                break;
            case "Reverse":
                _Reverse.enabled = false;
                break;
            case "StrongSlash":
                _strongSlash.enabled = false;
                break;
            case "BackSlash":
                _backSlash.enabled = false;
                break;
            case "FullSwing":
                _fullSwing.enabled = false;
                break;
            default:
                Debug.Log("どれにも該当しませんでした");
                break;
        }
    }


    //全てのコライダーをオフにする
    public void AllAttackColliderOff()
    {
        _superSlash.enabled=false;
        _speedySlash.enabled=false;
        _upSlash.enabled=false;
        _Reverse.enabled=false;
        _strongSlash.enabled=false;
        _backSlash.enabled=false;
        _fullSwing.enabled=false;

    }

    public void OnHitAttack(Collider collider)
    {
        //もし当たった対象のレイヤーが13の「Enemy」なら
        if (collider.gameObject.layer == 13)
        {
            //str*攻撃倍率の数値を与える
            int damagePoint = (int)(_playerStatus.Power * _attackValue);
            //さらにランダム要素を加える
            damagePoint  =(int)(damagePoint* Random.Range(0.8f, 1.2f));
            //攻撃範囲に入った敵のステータスを取得
            var targetMob = collider.GetComponent<EnemyStatus>();
            //もし攻撃範囲に敵が居なければ何もしない
            if (null == targetMob) return;
            targetMob.Damage(damagePoint);
            HitSoundSelecter();

            var obj = Instantiate<GameObject>(damageUI, collider.bounds.center - Camera.main.transform.forward * 0.2f, Quaternion.identity);
            obj.GetComponent<EnemyDamageText>().damageText.text = damagePoint.ToString();


        }

    }


    public void HitSoundSelecter()
    {
        switch (clipName)
        {
            case "SuperSlash":
                _playerSoundsScript.PlayHitSound("Hit1");
                break;
            case "SpeedySlash":
                _playerSoundsScript.PlayHitSound("Hit1");
                break;
            case "UpSlash":
                _playerSoundsScript.PlayHitSound("Hit2");
                break;
            case "Reverse":
                _playerSoundsScript.PlayHitSound("Hit2");
                break;
            case "StrongSlash":
                _playerSoundsScript.PlayHitSound("Hit3");
                break;
            case "BackSlash":
                _playerSoundsScript.PlayHitSound("Hit3");
                break;
            case "FullSwing":
                _playerSoundsScript.PlayHitSound("Hit4");
                break;
            default:
                Debug.Log("どれにも該当しませんでした");
                break;
        }

    }


    public void AttackVoiceAndSoundSelecter()
    {
        //1から3をランダムで選択
        int attackNum = (int)Random.Range(1.0f,3.99f);
        int swingNum=(int)Random.Range(1.0f, 2.99f);
        switch (attackNum)
        {
            case 1:
                _playerSoundsScript.PlayVoice("AttackVoice1");
                break;
            case 2:
                _playerSoundsScript.PlayVoice("AttackVoice2");
                break;
            case 3:
                _playerSoundsScript.PlayVoice("AttackVoice3");
                break;
        }

        switch (swingNum)
        {
            case 1:
                _playerSoundsScript.PlayActionSound("Swing1");
                break;
            case 2:
                _playerSoundsScript.PlayActionSound("Swing2");
                break;
        }








    }

















}










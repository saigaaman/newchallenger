﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
#pragma warning disable 0649
#pragma warning disable 0414
    CharacterController _controller;
    PlayerStatus _playerStatus;

    //サウンド
    [SerializeField]
    private GameObject _playerSoundObject;
    PlayerSoundsScript _playerSoundsScript;



    //特殊攻撃オブジェクト
    public GameObject Cyclone;
    public GameObject FireBurst;


    //アタック制御
    Animator _animator;
    private AttackAnim _attackAnim;
    private Attack1 _attack1;
    private Attack2 _attack2;
    private Attack3 _attack3;
    private Attack4 _attack4;
    private Attack5 _attack5;
    private Attack6 _attack6;
    private Attack7 _attack7;
    private CyCloneAnime _cyCloneAnime;
    private FireBurstAnime _fireBurstAnime;
    private SpAttack _spAttack;//まだ使ってない

    private Damage _damage;
    private Die _die;
    //通常攻撃時の移動係数
    private float _attackMove;
    //通常攻撃中の方向転換制御
    private bool _attackLookAt;
    public bool _AttackLookAt
    {
        get
        {
            return _attackLookAt;
        }
        set
        {
            _attackLookAt = value;
        }
    }


    LockOn _lockOn;

    private bool _wellGetThere = false;//ダッシュ攻撃のターゲットに到着しているか否か

    private Vector3 _moveDirection;//ユニティちゃんの実際の移動
    private Vector3 _jumpVelocity;//ユニティちゃんのジャンプ力


    void Start()
    {
        _animator = GetComponent<Animator>();//アニメーションを取得
        _attackAnim = _animator.GetBehaviour<AttackAnim>();
        _attack1 = _animator.GetBehaviour<Attack1>();
        _attack2 = _animator.GetBehaviour<Attack2>();
        _attack3 = _animator.GetBehaviour<Attack3>();
        _attack4 = _animator.GetBehaviour<Attack4>();
        _attack5 = _animator.GetBehaviour<Attack5>();
        _attack6 = _animator.GetBehaviour<Attack6>();
        _attack7 = _animator.GetBehaviour<Attack7>();
        _cyCloneAnime = _animator.GetBehaviour<CyCloneAnime>();
        _fireBurstAnime = _animator.GetBehaviour<FireBurstAnime>();
        _spAttack = _animator.GetBehaviour<SpAttack>();

        _damage = _animator.GetBehaviour<Damage>();
        _die = _animator.GetBehaviour<Die>();

        _controller = GetComponent<CharacterController>();//キャラコンを取得
        //GameObject player = GameObject.Find("TwohandsUnitychan");//必要なら使うオブジェクト検索
        _playerStatus = GetComponent<PlayerStatus>();
        _lockOn = GetComponent<LockOn>();

        //サウンドをセット
        _playerSoundsScript = _playerSoundObject.GetComponent<PlayerSoundsScript>();

    }


    void Update()
    {
        if (_die._isDie || _damage._isDamage) return;
        //アニメーションで攻撃してるか否か　そうでなければ通常の移動判定へ
        if (_attack1._isAttack1 || _attack2._isAttack2 || _attack3._isAttack3 || _attack4._isAttack4 || _attack5._isAttack5 || _attack6._isAttack6 || _attack7._isAttack7)
        {
            AttackMove();
        //魔法攻撃中は前進しない
        }else if (_cyCloneAnime._isCyClone || _fireBurstAnime._isFireBurst)
        {

        }
        else
        {
            //もし特殊移動等でキャラコンがオフになっていたら
            if (_controller.enabled == false)
            {
                _controller.enabled = true;
            }
            //地上なら
            if (_controller.isGrounded == true)
            {

                _playerStatus.MoveSpeed = 10.0f;
                //移動計算

                //前後キーが押されたらfroward変数にメインカメラの左右値を入力（x,z座標のみ）
                Vector3 forward = Vector3.Scale(Camera.main.transform.forward, new Vector3(1, 0, 1));
                //もし左右いずれかのキーが押されたらforward とrightに値を入れる
                Vector3 right = Vector3.Scale(Camera.main.transform.right, new Vector3(1, 0, 1));
                //その移動方向情報をmoveDirectionに渡し、キャラのスピード値をかける
                _moveDirection = Input.GetAxis("Horizontal") * right + Input.GetAxis("Vertical") * forward;

                //移動方向にキャラを向けるようにする(Vector3を使用し、ポジションのxとyを合わせる)
                transform.LookAt(transform.position + new Vector3(_moveDirection.x, 0, _moveDirection.z));

                //移動計算ここまで

                if (Input.GetButtonDown("Cross"))
                {
                    //ジャンプボタンが押されているなら
                    //ジャンプ数値をセット
                    _jumpVelocity.y = 6f;
                    Debug.Log("ジャンプ");
                    _animator.SetTrigger("Jump");
                    _playerSoundsScript.PlayVoice("AttackVoice2");
                    //soundScript.PlayJumpVoice();
                }
                //アクションコマンドここから
                //地上アクションボタンを受け付け
                //もし何かしらのキーが押されていれば
                AnyKeyDown();

            }
            //空中での処理
            else
            {
                //animator.SetBool("Grounded",false);//アニメーションに空中滑空状態を送る（必要になった場合）

                //カメラから見たZ座標方向を設定
                Vector3 forward = Vector3.Scale(Camera.main.transform.forward, new Vector3(1, 0, 1));
                //カメラから見たX座標方向を設定
                Vector3 right = Vector3.Scale(Camera.main.transform.right, new Vector3(1, 0, 1));

                //その移動方向情報に対し入力キーによる微妙な滑空を効かせる
                _moveDirection += (Input.GetAxis("Horizontal") * right) * 0.2f + (Input.GetAxis("Vertical") * forward) * 0.2f;
                //ロックオン時のみ、空中方向転換をしない
                //空中時のみ、重力をかける
                _jumpVelocity.y += Physics.gravity.y * Time.deltaTime;
                transform.LookAt(transform.position + new Vector3(_moveDirection.x, 0, _moveDirection.z));

            }


            _moveDirection = _moveDirection.normalized;
            _moveDirection *= _playerStatus.MoveSpeed;//移動値にスピードをかける
            _moveDirection.y = _jumpVelocity.y;//その後、ジャンプ数値を加える

            //移動値によりアニメーションを送る
            if (_moveDirection.z != 0 || _moveDirection.x != 0)
            {
                //アニメ側に移動速度を送る
                _animator.SetFloat("Speed", _playerStatus.MoveSpeed * 0.15f);
            }
            else
            {
                _animator.SetFloat("Speed", _playerStatus.MoveSpeed * 0.0f);
            }


            //最後にキャラクターオブジェクトを動かし、位置を確定（ちょうど１フレーム分）
            _controller.Move(_moveDirection * Time.deltaTime);

            _animator.SetBool("Grounded", _controller.isGrounded);
        }

    }


    //入力を受け付けるメソッド。一般的に攻撃などの行動出来る際はこれを使用
    private void AnyKeyDown()
    {
        if (Input.anyKeyDown)
        {
            //通常攻撃ボタンが押されているなら
            if (Input.GetButtonDown("Square"))
            {
                if (_playerStatus.Ap <= 0) return;
                _animator.SetTrigger("Attack");
            }


            //もし特殊攻撃ボタンが押されているなら
            if (Input.GetButtonDown("Triangle"))
            {
                Debug.Log("Triangle");
                if (_attack1._isAttack1)
                {
                    if (_playerStatus.Mp < 70) return;
                    _animator.SetTrigger("SpAttack");
                }
                else if (_attack2._isAttack2)
                {
                    if (_playerStatus.Mp < 100) return;
                    _animator.SetTrigger("SpAttack");
                }

            }

            //〇ボタンが押されているなら
            if (Input.GetButtonDown("Circle"))
            {
                Debug.Log("Circle");
            }


            //必殺技ボタンが押されているなら
            if (Input.GetButtonDown("L1"))
            {

                Debug.Log("L1");
            }

            //もしガードボタンが押されているなら
            if (Input.GetButtonDown("R1"))
            {
                Debug.Log("R1");
            }

        }




    
    }

    private void AttackMove()
    {
        //もし特殊移動等でキャラコンがオフになっていたら
        if (_controller.enabled == false)
        {
            _controller.enabled = true;
        }
        //移動の値を毎度リセット 
        _moveDirection = Vector3.zero;
        //Speedをアニメーターに反映
        //_animator.SetFloat("Move", 2.0f);
        //キャラの向いている方向に進ませる為の方向情報（今回は直進）
        Vector3 forward = Vector3.Scale(this.transform.forward, new Vector3(1, 0, 1));
        //移動情報を格納（今回は直進のみ）
        _moveDirection = forward;
        //移動情報を最適化
        _moveDirection = _moveDirection.normalized;
        //移動速度を計算
        _attackMove = AttackMoveSpeed();
        //移動値にスピードをかける
        _moveDirection *= _playerStatus.MoveSpeed * _attackMove;
        //移動情報を元に移動させる
        _controller.Move(_moveDirection * Time.deltaTime);
        //_moveDirection = Vector3.zero;
        AnyKeyDown();
        //ActionRotaChange();

    }




    //アクション遷移ちゅうにピンポイントでキャラの向いている方向を変える為のメソッド　
    //最終的に.Moveさせない事で、方向だけ向けさせ、攻撃開始時の方向制御に自由を効かせる
    public void ActionRotaChange()
    {

        //前後キーが押されたらfroward変数にメインカメラの左右値を入力（x,z座標のみ）
        Vector3 forward = Vector3.Scale(Camera.main.transform.forward, new Vector3(1, 0, 1));
        //もし左右いずれかのキーが押されたらforward とrightに値を入れる
        Vector3 right = Vector3.Scale(Camera.main.transform.right, new Vector3(1, 0, 1));
        //その移動方向情報をmoveDirectionに渡し、キャラのスピード値をかける
        _moveDirection = Input.GetAxis("Horizontal") * right + Input.GetAxis("Vertical") * forward;

        //移動方向にキャラを向けるようにする(Vector3を使用し、ポジションのxとyを合わせる)
        transform.LookAt(transform.position + new Vector3(_moveDirection.x, 0, _moveDirection.z));


    }

    private float AttackMoveSpeed()
    {
        if (_attack1._isAttack1)
        {
            _attackMove = 0.1f;
        }
        else if (_attack2._isAttack2)
        {
            _attackMove = 0.2f;
        }
        else if (_attack3._isAttack3)
        {
            _attackMove = 0.05f;
        }
        else if (_attack4._isAttack4)
        {
            _attackMove = 0.05f;
        }
        else if (_attack5._isAttack5)
        {
            _attackMove = 0.0f;
        }
        else if (_attack6._isAttack6)
        {
            _attackMove = 0.1f;
        }
        else if (_attack7._isAttack7)
        {
            _attackMove = 0.1f;
        }
        return _attackMove;
    }


    public void CyCloneOn()
    {
        _playerSoundsScript.PlayVoice("MagicVoice1");
        _playerStatus.DownMp(100);
        Instantiate(Cyclone, new Vector3(0.0f, 2.0f, 0.0f), Quaternion.identity);
    }

    public void FireBurstOn()
    {
        _playerSoundsScript.PlayVoice("MagicVoice1");
        _playerStatus.DownMp(70);
        Instantiate(FireBurst, new Vector3(0.0f, 2.0f, 0.0f), Quaternion.identity);
    }




}
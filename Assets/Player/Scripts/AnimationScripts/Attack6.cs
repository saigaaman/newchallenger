﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack6 : StateMachineBehaviour
{
    public bool _isAttack6 = false;

    public GameObject player;
    private PlayerAttack playerAttack;

    private void Awake()
    {
        playerAttack = player.GetComponent<PlayerAttack>();
    }




    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _isAttack6 = true;
    }



    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _isAttack6 = false;

        //もしアニメーションが中断された場合も含め、Exit時にコライダーを無効にする
        playerAttack.AllAttackColliderOff();
    }

}
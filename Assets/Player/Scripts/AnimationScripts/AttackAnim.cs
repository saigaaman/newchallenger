﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackAnim : StateMachineBehaviour
{
    public string triggerName1;
    public string triggerName2;

    //OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.ResetTrigger(triggerName1);
        animator.ResetTrigger(triggerName2);
    }

}

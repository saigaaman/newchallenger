﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatus : MonoBehaviour
{
#pragma warning disable 0649
#pragma warning disable 0414
    //このステータスは他の
    public static PlayerStatus _instance;

    public GameObject _uimanager;
    StatusGaugeManager _statusGaugeManager;
    SimpleHpGauge _simpleHpGauge;
    [SerializeField]
    private GameObject _playerSoundObject;
    PlayerSoundsScript _playerSoundsScript;



    //現在のアニメーション取得用
    Animator _animator;
    private Die _die;
    private Damage _damage;
    private Attack1 _attack1;
    private Attack2 _attack2;
    private Attack3 _attack3;
    private Attack4 _attack4;
    private Attack5 _attack5;
    private Attack6 _attack6;
    private Attack7 _attack7;

    //HP自動回復
    private float _hpMpRecoveryTime;
    //AP自動回復
    private float _apRecoveryTime;



    //移動速度
    private float moveSpeed;
    public float MoveSpeed
    {
        get
        {
            return moveSpeed;
        }
        set
        {
            moveSpeed = value;
        }
    }
    //レベルと、ステータス係数 （1.0が基礎数値）criは最大100で、各キャラ固定
    [SerializeField]private float level;//全ての数値に関わるレベル
    [SerializeField] private float str;//攻撃力と最大攻撃回数に関わる数値
    [SerializeField] private float con;//最大HPと最大攻撃回数に関わる数値
    [SerializeField] private float intel;//魔法攻撃力に関わる数値
    [SerializeField] private float wis;//最大MPに関わる数値
    [SerializeField] private float cri;//クリティカル率に関わる数値

    //hp=体力数値　mp=魔力数値　ap=連続攻撃可能数値
    //power=攻撃力　magic=魔法攻撃力
    [SerializeField] private float maxHp;
    public float MaxHP
    {
        get
        {
            return maxHp;
        }
        set
        {
            maxHp = value;
        }
    }

    private float hp;
    public float Hp
    {
        get
        {
            return hp;
        }
        set
        {
            hp = value;
        }
    }

    [SerializeField] private float maxAp;
    public float MaxAP
    {
        get
        {
            return maxAp;
        }
        set
        {
            maxAp = value;
        }
    }

    private float ap;
    public float Ap
    {
        get
        {
            return ap;
        }
        set
        {
            ap = value;
        }
    }

    [SerializeField] private float maxMp;
    public float MaxMP
    {
        get
        {
            return maxMp;
        }
        set
        {
            maxMp = value;
        }
    }

    private float mp;
    public float Mp
    {
        get
        {
            return mp;
        }
        set
        {
            mp = value;
        }
    }

    private float power;
    public float Power
    {
        get
        {
            return power;
        }
        set
        {
            power = value;
        }
    }



    private float magic;
    public float Magic
    {
        get
        {
            return magic;
        }
        set
        {
            magic = value;
        }
    }



    // Start is called before the first frame update
    private void Start()
    {
        _statusGaugeManager = _uimanager.GetComponent<StatusGaugeManager>();
        _simpleHpGauge = GetComponent<SimpleHpGauge>();

        //ステータスを初期値へ
        maxHp = (int)(level * con+(300*con)*10);
        hp = maxHp;
        maxAp = (int)(100 + (level * str) + (level * con));
        ap = maxAp;
        maxMp = (int)(level * wis+(100*wis)*10);
        mp = maxMp;

        power = (level * str)+(100*str);
        magic = (level * intel)+(100*str);


        //ゲージをセット
        _statusGaugeManager.HpGaugeUpdate(maxHp, hp);
        _statusGaugeManager.ApGaugeUpdate(maxAp, ap);
        _statusGaugeManager.MpGaugeUpdate(maxMp, mp);
        //現在のアニメーションの情報取得
        //アニメーションを取得
        _animator = GetComponent<Animator>();
        _attack1 = _animator.GetBehaviour<Attack1>();
        _attack2 = _animator.GetBehaviour<Attack2>();
        _attack3 = _animator.GetBehaviour<Attack3>();
        _attack4 = _animator.GetBehaviour<Attack4>();
        _attack5 = _animator.GetBehaviour<Attack5>();
        _attack6 = _animator.GetBehaviour<Attack6>();
        _attack7 = _animator.GetBehaviour<Attack7>();
        _die = _animator.GetBehaviour<Die>();
        //サウンドをセット
        _playerSoundsScript = _playerSoundObject.GetComponent<PlayerSoundsScript>();

    }


    private void Update()
    {
        if (_die._isDie) return;
        AutoHpMpRecovery();
        ApAutoRecovery();
    }






    //APの自然回復メソッド。UpDateメソッド内で実行させる
    private void ApAutoRecovery()
    {
        if (_attack1._isAttack1 || _attack2._isAttack2 || _attack3._isAttack3 || _attack4._isAttack4 || _attack5._isAttack5 || _attack6._isAttack6 || _attack7._isAttack7)
        {
            _apRecoveryTime = 0;

        }
        else
        {
            _apRecoveryTime += 1 * Time.deltaTime;
        }

        if (_apRecoveryTime >= 1 && ap < maxAp)
        {

            ap += (maxAp / 1.5f) * Time.deltaTime;
            if (ap > maxAp)
            {
                ap = maxAp;
            }
            //ゲージを更新
            _statusGaugeManager.ApGaugeUpdate(maxAp, ap);
        }
    }

    //MPとHPの自動回復メソッド。UpDateメソッド内で実行させる

    private void AutoHpMpRecovery()
    {
        //HPとMPが最大なら回復しない
        if (hp==maxHp&&mp==maxMp) {
            _hpMpRecoveryTime = 0;
        }
        else
        {
            _hpMpRecoveryTime += 1 * Time.deltaTime;
        }
        //10秒経過したら瞬間的に回復する
        if (_hpMpRecoveryTime >= 10)
        {
            hp += (maxHp / 10);
            mp += (maxMp / 20);
            if (hp>maxHp)
            {
                hp = maxHp;
            }
            if (mp > maxMp)
            {
                mp = maxMp;
            }
            //ゲージを更新
            _statusGaugeManager.HpGaugeUpdate(maxHp, hp);
            _statusGaugeManager.MpGaugeUpdate(maxMp, mp);
            _simpleHpGauge.HpGaugeUpdate(maxHp, hp);
            _hpMpRecoveryTime = 0;
        }

    }




    //スリップ無しの攻撃を受けた場合（受けるダメージ）
    public void Damage(int damagePoint)
    {
        if (_die._isDie) return;
        //ダメージと死亡したか否かの処理。他の処理と整理するため、HP処理はDownHpメソッドに投げる
        DownHp(damagePoint);

    }

    //ノックバックありのダメージを受けた状態（受けるダメージ　スリップ）
    public void DamageAndSlip(int damagePoint,float slip)
    {


    }

    //打ち上げスキルを受けてしまった状態　（受けるダメージ、打ちあがる高さ、スリップ）
    public void DamageAndUp(int damagePoint, float up,float slip)
    {


    }






    //減少値をセットする事で減算処理をしてくれるメソッド３種類
    public void DownHp(int value)
    {
        Debug.Log(""+value);
        hp -= value;
        if (hp <= 0)
        {
            hp =0;
            //死亡処理ToDo
            GetDie();
            _animator.SetTrigger("Die");
        }
        else
        {
            //ダメージボイスを出す
            GetHitDamageSelect();
            _animator.SetTrigger("Damage");
        }
        //ゲージを更新
        _statusGaugeManager.HpGaugeUpdate(maxHp, hp);
        _simpleHpGauge.HpGaugeUpdate(maxHp, hp);
    }

    public void DownAp(int value)
    {
        Ap -= value;
        if (Ap < 0)
        {
            Ap = 0;
            

        }
        //ゲージを更新
        _statusGaugeManager.ApGaugeUpdate(maxAp, ap);
    }


    public void DownMp(int value)
    {
        mp -= value;
        if (mp < 0)
        {
            mp = 0;
            //死亡処理ToDo

        }
        //ゲージを更新
        _statusGaugeManager.MpGaugeUpdate(maxMp, mp);
    }



    //回復量をセットする事で回復処理をしてくれるメソッド３種類
    public void RecoveryHp(int value)
    {
       hp += value;
        if (hp > maxHp)
        {
            hp = maxHp;
        }
        //ゲージを更新
        _statusGaugeManager.HpGaugeUpdate(maxHp,hp);
        _simpleHpGauge.HpGaugeUpdate(maxHp, hp);
    }

    public void RecoveryAp(int value)
    {
        ap += value;
        if (ap > maxAp)
        {
            ap = maxAp;
        }
        //ゲージを更新
        _statusGaugeManager.ApGaugeUpdate(maxAp, ap);
    }
    public void RecoveryMp(int value)
    {
        mp += value;
        if (mp > maxMp)
        {
            mp = maxMp;
        }
        //ゲージを更新
        _statusGaugeManager.MpGaugeUpdate(maxMp, mp);
    }


    //クリステータス＋アタックパワーでクリティカル成否、およびダメージ計算をするメソッド
    public void CriticalValue()
    {

    }


    //攻撃を受けた際の声を決めるメソッド
    public void GetHitDamageSelect()
    {
        int damageVoice =(int) Random.Range(1.0f, 3.99f);

        switch (damageVoice)
        {
            case 1:
                _playerSoundsScript.PlayVoice("DamageVoice1");
                break;
            case 2:
                _playerSoundsScript.PlayVoice("DamageVoice2");
                break;
            case 3:
                _playerSoundsScript.PlayVoice("DamageVoice3");
                break;
        }
    }

    public void GetDie()
    {
        int dieVoice = (int)Random.Range(1.0f, 2.99f);

        switch (dieVoice)
        {
            case 1:
                _playerSoundsScript.PlayVoice("DieVoice1");
                break;
            case 2:
                _playerSoundsScript.PlayVoice("DieVoice2");
                break;
        }


    }













}

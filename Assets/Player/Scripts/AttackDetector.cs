﻿using System;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider))]
public class AttackDetector : MonoBehaviour
{
#pragma warning disable 0649
    [SerializeField] private TriggerEvent onTriggerEnter = new TriggerEvent();
    [SerializeField] private TriggerEvent onTriggerStay = new TriggerEvent();
    //<summary>
    /// <summary>
    ///Is TriggerがONでほかのColliderと重なっているときは、このメソッドが常にコールされる
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        //指定された処理を実行する
        onTriggerEnter.Invoke(other);
    }

    private void OnTriggerStay(Collider other)
    {
        //指定された処理を実行する
        onTriggerStay.Invoke(other);

    }

    //UnityEventを継承したクラスに[Serializable]属性を付与する事で、Inspectorウィンドウ上に表示
    [Serializable]
    public class TriggerEvent : UnityEvent<Collider>
    {

    }



}


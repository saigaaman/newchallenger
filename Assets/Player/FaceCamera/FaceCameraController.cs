﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceCameraController : MonoBehaviour
{
    public GameObject POV;

    private void Start()
    {
    }


    // Update is called once per frame
    void Update()
    {
        transform.LookAt(POV.transform.position);
    }
}

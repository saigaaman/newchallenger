﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBurstMoover : MonoBehaviour
{
#pragma warning disable 0649
#pragma warning disable 0414
    private EffectSound _effectSound;

    private GameObject _player;

    private Rigidbody _rb;
    private float speed = 25f;//移動速度
    Vector3 _pos = new Vector3(0, 0, 0);//術者の座標を取得

    //ヒットしたかどうか
    private bool _hitOnOff;

    [SerializeField]
    private GameObject _burstSystem;



    // プレイヤーの向いている方向を確認しその方向に動く準備＋コルーチン回す
    void Awake()
    {
        _hitOnOff = false;
        //移動はゆっくりさせる
        _rb = GetComponent<Rigidbody>();
        //処理を軽くするため指定
        _player = GameObject.FindWithTag("Player");
        //プレイヤーの位置取得
        Transform Ptransform = _player.transform;
        //プレイヤーの向いているY方角だけ合わせる
        this.transform.localEulerAngles = Ptransform.localEulerAngles;
        //ボールをプレイヤーの位置へ
        _pos.x = Ptransform.position.x;
        _pos.y = Ptransform.position.y + 0.5f;
        _pos.z = Ptransform.position.z;
        this.transform.position = _pos;

    }


    private void Start()
    {

        //エフェクトの音楽
        _effectSound = GetComponent<EffectSound>();
        _effectSound.StartSound("ShotGunShoot");
        StartCoroutine("FireBurstEnd");
    }





    // 移動させるスクリプト。
    void Update()
    {
        if (!_hitOnOff)
        {
            _rb.MovePosition(transform.position + transform.forward * speed * Time.deltaTime);
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        string layerName = LayerMask.LayerToName(other.gameObject.layer);
        if (layerName == "Enemy")
        {
            _hitOnOff = true;

            GetComponent<MeshRenderer>().enabled=false;

            _burstSystem.SetActive(true);


            //5秒後に消える
            StartCoroutine("FireBurstTimeController");
        }


    }





    //ヒットすれば5秒後に消去する
    IEnumerator FireBurstTimeController()
    {

        //5秒停止
        yield return new WaitForSeconds(5);
        Destroy(this.gameObject);

    }

    //ヒットしなければ15秒後に消去する
    IEnumerator FireBurstEnd()
    {

        //5秒停止
        yield return new WaitForSeconds(15);
        Destroy(this.gameObject);

    }




}

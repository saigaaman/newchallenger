﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CycloneContoller : MonoBehaviour
{
    #pragma warning disable 0649
    #pragma warning disable 0414
    //　DamageUIプレハブ
    [SerializeField]
    private GameObject damageUI;
    private EffectSound _effectSound;

    private GameObject _player;
    private PlayerStatus _playerStatus;

    private Rigidbody _rb;
    private float speed=1.5f;//移動速度
    Vector3 _pos = new Vector3(0, 0, 0);//術者の座標を取得

    private GameObject _obj;//サイクロンシステムのエフェクトを入れる箱
    private GameObject _Cyclone;//サイクロンエフェクトオブジェ
    private float _DamageCount;


    // プレイヤーの向いている方向を確認しその方向に動く準備＋コルーチン回す
    void Awake()
    {
        _DamageCount = 0f;
        _Cyclone = GameObject.Find("Cyclone");

        //移動はゆっくりさせる
        _rb = GetComponent<Rigidbody>();
        //処理を軽くするため指定
        _player = GameObject.FindWithTag("Player");
        _playerStatus = _player.GetComponent<PlayerStatus>();
        //プレイヤーの位置取得
        Transform Ptransform = _player.transform;
        //竜巻をプレイヤーの位置へ
        _pos.x = Ptransform.position.x;
        _pos.y = Ptransform.position.y + 3.5f;
        _pos.z = Ptransform.position.z;
        this.transform.position = _pos;
        //プレイヤーの向いているY方角だけ合わせる
        this.transform.localEulerAngles = Ptransform.localEulerAngles;
        //5秒後に消える
        StartCoroutine("CycloneTimeController");
        //エフェクトのコピーを作成、子として登録、アクティブ状態にする
        _obj = Instantiate(_Cyclone, Ptransform.transform.position, this.transform.rotation);
        _obj.GetComponent<ParticleSystem>().Play();
        _obj.transform.localEulerAngles = new Vector3(90, Ptransform.localEulerAngles.y, 0);
        _obj.transform.parent = transform;
        _obj.SetActive(true);

    }


    private void Start()
    {
        //エフェクトの音楽
        _effectSound = GetComponent<EffectSound>();
        _effectSound.StartSound("CycloneSound");
    }





    // 移動させるスクリプト。
    //対象が触れている間は空中に浮かび上がりながら0.5秒ごとにダメージ（最初は0からカウント）最大10ヒット
    void Update()
    {
        _rb.MovePosition(transform.position + transform.forward * speed* Time.deltaTime);

    }

    public void OnTriggerStay(Collider other)
    {
        _DamageCount += 1 * Time.deltaTime;
        //0.5秒に一回ダメージ判定を起こす
        if (_DamageCount>=0.5f) {
            var targetMob = other.GetComponent<EnemyStatus>();


            string layerName = LayerMask.LayerToName(other.gameObject.layer);

            if (layerName == "Enemy")
            {
                //ダメージポイント作成　ダメージはマジック依存
                int damagePoint = (int)(_playerStatus.Magic * 1.1);
                //さらにランダム要素を加える
                damagePoint = (int)(damagePoint * Random.Range(0.8f, 1.2f));
                //ダメージを与える
                targetMob.Damage(damagePoint);
                //ダメージUIをインスタンス化し、さらにダメージをテキストに反映
                var obj = Instantiate<GameObject>(damageUI, other.bounds.center - Camera.main.transform.forward * 0.2f, Quaternion.identity);
                obj.GetComponent<EnemyDamageText>().damageText.text = damagePoint.ToString();
                _effectSound.HitSound("jump1");
            }

            _DamageCount = 0f;
        }
    }





    //5秒後に消去する
    IEnumerator CycloneTimeController()
    {

        //5秒停止
        yield return new WaitForSeconds(5);
        Destroy(this.gameObject);

    }


}

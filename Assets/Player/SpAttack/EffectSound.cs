﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class EffectSound : MonoBehaviour
{
#pragma warning disable 0649
#pragma warning disable 0414
    private AudioSource[] _audioSource;
    //エフェクト出現時点のサウンド
    public AudioClip[] _StartSound;
    //エフェクト全般のサウンド
    public AudioClip[] _Idle;
    //エフェクト命中時のサウンド
    public AudioClip[] _HitSound;

    private void Awake()
    {
        _audioSource = GetComponents<AudioSource>();
    }

    //ボイス(主に主人公や中ボス)
    public void StartSound(string clipName)
    {
        //FirstOrDefaultの場合、例外が発生しなければ規定値（intなら0）となる
        AudioClip selectSE = _StartSound.FirstOrDefault(n => n.name == clipName);

        _audioSource[0].clip = selectSE;
        _audioSource[0].Play();

    }

    //アクションする時のサウンド
    public void IdleSound(string clipName)
    {
        //FirstOrDefaultの場合、例外が発生しなければ規定値（intなら0）となる
        AudioClip selectSE = _Idle.FirstOrDefault(n => n.name == clipName);

        _audioSource[1].clip = selectSE;
        _audioSource[1].Play();

    }


    //攻撃命中用サウンド
    public void HitSound(string clipName)
    {
        //FirstOrDefaultの場合、例外が発生しなければ規定値（intなら0）となる
        AudioClip selectSE = _HitSound.FirstOrDefault(n => n.name == clipName);

        _audioSource[2].clip = selectSE;
        _audioSource[2].Play();

    }

}


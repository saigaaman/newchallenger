﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBurstController : MonoBehaviour
{
#pragma warning disable 0649
#pragma warning disable 0414
    //　DamageUIプレハブ
    [SerializeField]
    private GameObject damageUI;
    private EffectSound _effectSound;

    private GameObject _player;
    private PlayerStatus _playerStatus;


    private GameObject _fireBurst;//ファイヤーバーストオブジェクト
    private GameObject _obj;//ファイヤバーストシステムのエフェクトを入れる箱
    private GameObject _fireBall;//ファイヤバーストエフェクトオブジェ
    private float _DamageCount;
    Vector3 _pos = new Vector3(0, 0, 0);//爆発地点の座標を取得


    // プレイヤーの向いている方向を確認しその方向に動く準備＋コルーチン回す
    void Awake()
    {
        _DamageCount = 0f;
        _fireBall = GameObject.Find("FireBall");
        //一つ上の親オブジェクトを取得
        _fireBurst = transform.root.gameObject;

        //処理を軽くするため指定
        _player = GameObject.FindWithTag("Player");
        _playerStatus = _player.GetComponent<PlayerStatus>();


        //現在の親オブジェクトの位置を取得
       
        Transform Ftransform = _fireBurst.transform;
        //爆発を親オブジェクトの位置へ
        _pos.x = Ftransform.position.x;
        _pos.y = Ftransform.position.y;
        _pos.z = Ftransform.position.z;
        this.transform.position = _pos;
        //親オブジェクトの向いているY方角だけ合わせる
        this.transform.localEulerAngles = Ftransform.localEulerAngles;



        //エフェクトのコピーを作成、子として登録、アクティブ状態にする
        _obj = Instantiate(_fireBall, _fireBurst.transform.position, this.transform.rotation);
        _obj.GetComponent<ParticleSystem>().Play();
        _obj.transform.localEulerAngles = new Vector3(90, Ftransform.localEulerAngles.y, 0);
        _obj.transform.parent = transform;
        _obj.SetActive(true);

    }


    private void Start()
    {
        //エフェクトの音楽
        _effectSound = GetComponent<EffectSound>();
        _effectSound.StartSound("FireBurstStart");
    }

    public void OnTriggerEnter(Collider other) { 

            var targetMob = other.GetComponent<EnemyStatus>();


            string layerName = LayerMask.LayerToName(other.gameObject.layer);

            if (layerName == "Enemy")
            {
                //ダメージポイント作成　ダメージはマジック依存
                int damagePoint = (int)(_playerStatus.Magic * 5.0);
                //さらにランダム要素を加える
                damagePoint = (int)(damagePoint * Random.Range(0.8f, 1.2f));
                //ダメージを与える
                targetMob.Damage(damagePoint);
                //ダメージUIをインスタンス化し、さらにダメージをテキストに反映
                var obj = Instantiate<GameObject>(damageUI, other.bounds.center - Camera.main.transform.forward * 0.2f, Quaternion.identity);
                obj.GetComponent<EnemyDamageText>().damageText.text = damagePoint.ToString();
            }



    }


    public void OnTriggerStay(Collider other)
    {
        _DamageCount += 1 * Time.deltaTime;
        //1秒に一回燃焼ダメージ判定を起こす
        if (_DamageCount >= 1f)
        {
            var targetMob = other.GetComponent<EnemyStatus>();


            string layerName = LayerMask.LayerToName(other.gameObject.layer);

            if (layerName == "Enemy")
            {
                //ダメージポイント作成　ダメージはマジック依存
                int damagePoint = (int)(_playerStatus.Magic * 1.5);
                //さらにランダム要素を加える
                damagePoint = (int)(damagePoint * Random.Range(0.8f, 1.2f));
                //ダメージを与える
                targetMob.Damage(damagePoint);
                //ダメージUIをインスタンス化し、さらにダメージをテキストに反映
                var obj = Instantiate<GameObject>(damageUI, other.bounds.center - Camera.main.transform.forward * 0.2f, Quaternion.identity);
                obj.GetComponent<EnemyDamageText>().damageText.text = damagePoint.ToString();
                _effectSound.HitSound("HeavyHit");
            }

            _DamageCount = 0f;
        }
    }





 
}

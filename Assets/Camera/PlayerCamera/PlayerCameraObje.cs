﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCameraObje : MonoBehaviour
{
    //プレイヤーの距離
    public GameObject _player;
    //プレイヤーとの距離の差分
    private float _distance;

    //最終的な横回転目標値
    float _rotaYAngles;
    //マウスから取得した回転値
    float _yAngles;



    private void Update()
    {
        //左クリック中
        if (Input.GetMouseButton(0))
        {
            //マウスドラッグでX座標とY座標を検知
            //マウスで取る横座標はｘだが、回転座標上はyを用いる
            //横回転
            _yAngles += Input.GetAxis("Mouse X")*10;
            //マウスで取る横座標はyだが、回転座標上はxを用いる

        }
        //横の回転目標を設定
        _rotaYAngles = transform.rotation.y + _yAngles;




        //指定した方向にゆっくり回転する場合
        transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(transform.rotation.x, _rotaYAngles, 0), 300f * Time.deltaTime);



    }








    // プレイヤーを追従するプログラム
    void FixedUpdate()
    {
        //距離の差分からLerpで近づく速度を調整する
        _distance = Vector3.Distance(this.transform.position, _player.transform.position);
        if (_distance < 0)
        {
            this.transform.position = Vector3.Lerp(this.transform.position, _player.transform.position, 8.0f* Time.deltaTime);
        }
        else if (_distance<=1.0f) {
            this.transform.position = Vector3.Lerp(this.transform.position, _player.transform.position, 6.5f * Time.deltaTime);
        }
        else if (_distance <= 3.0f)
        {
            this.transform.position = Vector3.Lerp(this.transform.position, _player.transform.position, 5.0f*Time.deltaTime);
        }else
        {
            this.transform.position = Vector3.Lerp(this.transform.position, _player.transform.position, 3.0f * Time.deltaTime);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
public class EffectController : MonoBehaviour
{

    //エフェクトを操作する為のスクリプト。
    //このクラスをインスタンス化し、シーンが変わってもオブジェクトが残るようにする
    public static EffectController _instance;
    //パーティクルシステムを配列にし、格納する。
    public GameObject[] _particleSystem;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            //シーンが変わっても残す
            DontDestroyOnLoad(gameObject);

        }
        else
        {
            //もし２つ存在する場合はこのオブジェクトを消す
            //（あくまで存在しない場合のみシーンが変わっても残る）
            Destroy(gameObject);
        }
    }//Awake

    //エフェクトを呼ぶためのメソッド。引数はエフェクト名
    public void EffectLoader(string effectName)
    {
        //パーティクルシステム内の要素を検索し、該当の要素をselectEffectに代入。
        //※FirstOrDefaultとは要素を検索するLINQで、最初に見つかったものだけ持ってくる。
        //この場合、入力されたエフェクトネームと一致しているものを引っ張ってきている。
        //ちなみにFirstOrDefaultの場合、見つからなければ規定値（intなら0）となる
        GameObject selectEffect = _particleSystem.FirstOrDefault(n => n.name == effectName);

        //対象のエフェクトを呼び出す
        selectEffect.SetActive(true);

    }

    //エフェクトを消す為のメソッド。引数はエフェクト名
    public void EffectDestroyer(string effectName)
    {
        
        GameObject selectEffect = _particleSystem.FirstOrDefault(n => n.name == effectName);

        //対象のエフェクトをオフにする
        selectEffect.SetActive(false);
    }




}

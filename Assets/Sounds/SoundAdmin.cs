﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SoundAdmin : MonoBehaviour
{
    public static SoundAdmin _instance;
    private AudioSource[] _audioSource;
    public AudioClip[] _Music;
    //public AudioClip[] _Voice;
    //public AudioClip[] _Sound;
    public AudioClip[] _Other;


    private void Awake()
    {
        if (_instance==null)
        {
            _instance = this;
            //0 Music
            //1 Voice
            //2 Sound
            _audioSource = GetComponents<AudioSource>();
            DontDestroyOnLoad(gameObject);

        }
        else
        {
            Destroy(gameObject);
        }
    }//Awake

    private void Start()
    {
        PlayMusic("通常戦闘");
    }



    //メイン音楽を鳴らす時
    public void PlayMusic(string clipName)
    {
        //FirstOrDefaultの場合、例外が発生しなければ規定値（intなら0）となる
        AudioClip selectSE = _Music.FirstOrDefault(n => n.name == clipName);
        if (selectSE!=null)
        {
            _audioSource[0].clip = selectSE;
            _audioSource[0].Play();
        }
    }
    /*
    //ボイス(主に主人公や中ボス)
    public void PlayVoice(string clipName)
    {
        //FirstOrDefaultの場合、例外が発生しなければ規定値（intなら0）となる
        AudioClip selectSE = _Voice.FirstOrDefault(n => n.name == clipName);

            _audioSource[1].clip = selectSE;
            _audioSource[1].Play();
        
    }

    //サウンド全般
    public void PlaySound(string clipName)
    {
        //FirstOrDefaultの場合、例外が発生しなければ規定値（intなら0）となる
        AudioClip selectSE = _Sound.FirstOrDefault(n => n.name == clipName);

        _audioSource[2].clip = selectSE;
        _audioSource[2].Play();

    }


    //エフェクト用ワンショットサウンド
    public void PlayEffectSound(string clipName)
    {
        //FirstOrDefaultの場合、例外が発生しなければ規定値（intなら0）となる
        AudioClip selectSE = _Sound.FirstOrDefault(n => n.name == clipName);

        _audioSource[2].clip = selectSE;
        _audioSource[2].PlayOneShot(selectSE);

    }

    */




    //Other（他の特殊音全て）
    public void PlayOther(string clipName)
    {
        //FirstOrDefaultの場合、例外が発生しなければ規定値（intなら0）となる
        AudioClip selectSE = _Other.FirstOrDefault(n => n.name == clipName);

        _audioSource[1].clip = selectSE;
        _audioSource[1].Play();

    }











}

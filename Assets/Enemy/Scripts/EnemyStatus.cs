﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStatus : MonoBehaviour
{

#pragma warning disable 0649
#pragma warning disable 0414


    //現在のアニメーション取得用
    Animator _animator;
    //private Attack1 _attack1;
    private Die _die;
    public GameObject _enemyStock;

    EnemySimpleHpGauge _enemySimpleHpGauge;

    //移動速度
    private float moveSpeed;
    public float MoveSpeed
    {
        get
        {
            return moveSpeed;
        }
        set
        {
            moveSpeed = value;
        }
    }
    //レベルと、ステータス係数 （1.0が基礎数値）criは最大100で、各キャラ固定
    [SerializeField] private float level;//全ての数値に関わるレベル
    [SerializeField] private float str;//攻撃力と最大攻撃回数に関わる数値
    [SerializeField] private float con;//最大HPと最大攻撃回数に関わる数値
    [SerializeField] private float intel;//魔法攻撃力に関わる数値
    [SerializeField] private float cri;//クリティカル率に関わる数値

    //hp=体力数値　mp=魔力数値　ap=連続攻撃可能数値
    //power=攻撃力　magic=魔法攻撃力
    [SerializeField] private float maxHp;
    public float MaxHP
    {
        get
        {
            return maxHp;
        }
        set
        {
            maxHp = value;
        }
    }

    private float hp;
    public float Hp
    {
        get
        {
            return hp;
        }
        set
        {
            hp = value;
        }
    }



    private float power;
    public float Power
    {
        get
        {
            return power;
        }
        set
        {
            power = value;
        }
    }



    private float magic;
    public float Magic
    {
        get
        {
            return magic;
        }
        set
        {
            magic = value;
        }
    }



    private void Awake()
    {
        //ステータスを初期値へ
        maxHp = (int)(level * con + (300 * con) * 10);
        hp = maxHp;

        power = (level * str) + (100 * str);
        magic = (level * intel) + (100 * str);

        //現在のアニメーションの情報取得
        //アニメーションを取得
        _animator = GetComponent<Animator>();
        _die = _animator.GetBehaviour<Die>();
        _enemySimpleHpGauge = GetComponent<EnemySimpleHpGauge>();
        //ゲージを更新
        _enemySimpleHpGauge.HpGaugeUpdate(maxHp, hp);
    }


    //スリップ無しの攻撃を受けた場合（受けるダメージ）
    public void Damage(int damagePoint)
    {

        //ダメージと死亡したか否かの処理。他の処理と整理するため、HP処理はDownHpメソッドに投げる
        DownHp(damagePoint);


    }

    //ノックバックありのダメージを受けた状態（受けるダメージ　スリップ）
    public void DamageAndSlip(int damagePoint, float slip)
    {


    }

    //打ち上げスキルを受けてしまった状態　（受けるダメージ、打ちあがる高さ、スリップ）
    public void DamageAndUp(int damagePoint, float up, float slip)
    {


    }







    //減少値をセットする事で減算処理をしてくれるメソッド３種類
    public void DownHp(int value)
    {
        //死んでいたら何もさせない
        if (_die._isDie) return;

        hp -= value;


        if (hp <= 0)
        {
            hp = 0;
            //死亡処理ToDo
            _animator.SetTrigger("Die");
            Die();
            //15は死亡状態のオブジェクト
            this.gameObject.layer = 15;
        }
        else
        {
            _animator.SetTrigger("Damage");
        }
        //ゲージを更新
        _enemySimpleHpGauge.HpGaugeUpdate(maxHp, hp);
    }

   

    //回復量をセットする事で回復処理をしてくれるメソッド３種類
    public void RecoveryHp(int value)
    {
        hp += value;
        if (hp > maxHp)
        {
            hp = maxHp;
        }
        _enemySimpleHpGauge.HpGaugeUpdate(maxHp, hp);
    }

  


    //攻撃基礎値を計算してくれるメソッド
    public void AttackPowerVelue()
    {

    }

    //クリステータス＋アタックパワーでクリティカル成否、およびダメージ計算をするメソッド
    public void CriticalValue()
    {

    }


    //死亡処理
    public void Die()
    {
        StartCoroutine("DieCommand");
        float enemyLocx = Random.Range(0.0f, 10.0f);
        float enemyLocz = Random.Range(0.0f, 10.0f);
        Instantiate(_enemyStock, new Vector3(enemyLocx, 0f, enemyLocz), Quaternion.identity);
    }

    IEnumerator DieCommand()
    {
        //3秒停止
        yield return new WaitForSeconds(3);
        Destroy(this.gameObject);
    }











}

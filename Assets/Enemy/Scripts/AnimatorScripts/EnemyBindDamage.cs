﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBindDamage : StateMachineBehaviour
{
#pragma warning disable 0649
#pragma warning disable 0414
    private bool isMoveDamage = false;



    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        isMoveDamage = true;
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        isMoveDamage = false;
    }


}
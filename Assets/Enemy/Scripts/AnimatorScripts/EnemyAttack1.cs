﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack1 : StateMachineBehaviour
{
    public bool _isAttack1 = false;

    public GameObject enemy;
    private EnemyAttack enemyAttack;

    private void Awake()
    {
        enemyAttack = enemy.GetComponent<EnemyAttack>();
    }


    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _isAttack1 = true;
    }



    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _isAttack1 = false;

        //もしアニメーションが中断された場合も含め、Exit時にコライダーを無効にする
        enemyAttack.AllAttackColliderOff();
    }



}

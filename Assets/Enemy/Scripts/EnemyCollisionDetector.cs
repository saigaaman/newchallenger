﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

//※[RequireComponent(typeof(Collider))]
//必要なコンポーネントを入れたい場合に使用、今回はコライダーをアタッチしてくれる
//今回は別になくてもいいが・・・。
[RequireComponent(typeof(Collider))]
public class EnemyCollisionDetector : MonoBehaviour
{
    //
    [SerializeField] private TriggerEvent onTriggerEnter = new TriggerEvent();
    [SerializeField] private TriggerEvent onTriggerStay = new TriggerEvent();
    //<summary>
    /// <summary>
    ///Is TriggerがONでほかのColliderと重なっているときは、このメソッドが常にコールされる
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        //インスペクタ―上で指定された処理を実行する
        onTriggerEnter.Invoke(other);
    }

    private void OnTriggerStay(Collider other)
    {
        //インスペクタ―上で指定された処理を実行する
        onTriggerStay.Invoke(other);

    }

    //UnityEventを継承したクラスに[Serializable]属性を付与する事で、Inspectorウィンドウ上に表示
    [Serializable]
    public class TriggerEvent : UnityEvent<Collider>
    {

    }



}

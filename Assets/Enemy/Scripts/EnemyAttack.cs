﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
#pragma warning disable 0649
#pragma warning disable 0414
    //　DamageUIプレハブ
    [SerializeField]
    private GameObject damageUI;

    //攻撃のコライダー
    public Collider _attackCollider;

    //アニメーション
    Animator _animator;
    //攻撃全般。中断したら特定の処理をさせるためのもの
    private AttackAnim _attackAnim;
    //攻撃中アニメ
    private EnemyAttack1 _attack1;
    //ダメージアニメ
    private EnemyDamage _damage;
    //死亡アニメ
    private Die _die;

    //ダメージを与える為の双方のステータス
    PlayerStatus _playerStatus;
    EnemyStatus _enemyStatus;
    //自身を動かす為のスクリプト
    EnemyController _enemyController;

    //攻撃倍率
    private float _attackValue;
    //攻撃までどれくらい停止するか
    private float _waitAttack;
    //攻撃までのカウント
    private float _waitTime;


    //音
    EnemySoundScript _enemySoundScript;


    private void Start()
    {
        //現在のアニメーションの情報取得
        //アニメーションを取得
        _animator = GetComponent<Animator>();
        _attackAnim = _animator.GetBehaviour<AttackAnim>();
        _attack1 = _animator.GetBehaviour<EnemyAttack1>();
        _die = _animator.GetBehaviour<Die>();
        _damage= _animator.GetBehaviour<EnemyDamage>();

        //自身のステータスを取得
        _enemyStatus = GetComponent<EnemyStatus>();
        //自身のコントローラーを取得
        _enemyController = GetComponent<EnemyController>();
        //初期攻撃待機時間は1秒。その後はランダムとする
        _waitAttack = 1.0f;
        //自コンポーネント内のサウンドマネージャーを取得
        _enemySoundScript = GetComponent<EnemySoundScript>();
    }


    //攻撃対象が攻撃範囲に入った場合に呼ばれるメソッド
    public void OnAttackRangeEnter(Collider collider)
    {
        //攻撃中もしくは自分が死んでいたらなら何もしない
        if (_attack1._isAttack1||_die._isDie) return;
        //もしダメ―ジを受けてしまったら
        if (_damage._isDamage)
        {
            //攻撃までのカウントダウンをリセットする
            _waitTime = 0;
        }
        //攻撃範囲に入った対象のレイヤーが8の「Player」なら
        if (collider.gameObject.layer == 8)
        {
            //カウントダウンを加算、数値がランダムの待機時間に達したら
            _waitTime += 1 * Time.deltaTime;
            if (_waitTime >= _waitAttack)
            {
                //攻撃を行い、またカウントダウンをする
                _animator.SetTrigger("Attack");
                _waitTime = 0;
            }
        }
    }


    //攻撃判定の起点となるメソッド
    public void HitOn()
    {
        Debug.Log("敵攻撃開始");
        //攻撃倍率
        _attackValue = 1.0f;
        //スライムの音を出す
        _enemySoundScript.PlayActionSound("slime1");
        //攻撃コライダーをオンにして判定を出す
        _attackCollider.enabled = true;
        //攻撃するたびに攻撃間隔をリセット（待機時間を設定する）
        _waitAttack = Random.Range(0.3f,2.0f);
    }

    //攻撃判定を終了する際のメソッド
    public void HitOff()
    {
        Debug.Log("敵攻撃終了");
        _attackCollider.enabled = false;

    }


    //全てのコライダーをオフにする
    public void AllAttackColliderOff()
    {
        _attackCollider.enabled = false;


    }

    //攻撃がヒットした際のメソッド
    public void OnHitAttack(Collider collider)
    {
        //もし当たった対象のレイヤーが8の「Player」なら
        //※当たり判定はコライダーを持っているオブジェクトのレイヤーで判断する。
        //つまり、この場合プレイヤーのレイヤーを持っているオブジェクト内で処理をする
        if (collider.gameObject.layer == 8)
        {
            //str*攻撃倍率の数値を与える
            int damagePoint = (int)(_enemyStatus.Power * _attackValue);
            //さらにランダム要素を加える
            damagePoint = (int)(damagePoint * Random.Range(0.8f, 1.2f));
            //攻撃範囲に入った敵のステータスを取得
            var targetMob = collider.GetComponent<PlayerStatus>();
            //もし攻撃範囲に敵が居なければ何もしない
            if (null == targetMob) return;
            //ターゲットにダメージを与える
            targetMob.Damage(damagePoint);
            //ダメージUIのプレファブを呼び出す。引数は（対象オブジェ,位置,回転)
            //※位置は対象オブジェの中心部を設定し、さらにそれからカメラから見て手前の位置にしている。
            //オブジェクト.bounds.center　中央指定　　そこからマイナスしている結果
            //- Camera.main.transform.forward * 0.2f 　カメラから見て、該当オブジェクトの0.2f手前の位置となる
            var obj = Instantiate<GameObject>(damageUI, collider.bounds.center - Camera.main.transform.forward * 0.2f, Quaternion.identity);
            obj.GetComponent<DamageText>().damageText.text = damagePoint.ToString();
        }
    }



}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
#pragma warning disable 0649
#pragma warning disable 0414
    //追いかけるターゲット
    private GameObject _target;
    //アクティブモードオンかオフか　現状未使用
    //private bool _activeMode;
    //キャラコン
    CharacterController _enemyController;

    //アニメーション
    private Animator _animator;
    private EnemyAttack1 _attack1;
    private Die _die;
    private EnemyDamage _damage;

    //移動速度
    [SerializeField]
    private float _moveSpeed = 1.0f;
    //　実際の移動値
    private Vector3 _velocity;
    //　移動方向
    private Vector3 _direction;
    private Vector3 _lookPosition;
    //　到着フラグ
    private bool _arrived;


    void Start()
    {
        _enemyController = GetComponent<CharacterController>();
        _animator = GetComponent<Animator>();
        _attack1 = _animator.GetBehaviour<EnemyAttack1>();
        _die = _animator.GetBehaviour<Die>();
        _damage= _animator.GetBehaviour<EnemyDamage>();
        _velocity = Vector3.zero;
        _arrived = false;
        _target = GameObject.FindGameObjectWithTag("Player");
    }


    void Update()
    {
        //死んでいる・攻撃している・ダメージを受けている場合は動かさない
        if (_die._isDie||_attack1._isAttack1||_damage._isDamage) return;
        //向けるための位置を代入、今回は対象のX&Z座標(Y座標を弄ると飛び上がったりするので判定しない)
        _lookPosition = new Vector3(_target.transform.position.x, this.transform.position.y, _target.transform.position.z);
        transform.LookAt(_lookPosition);
        //まだ対象の傍に着いていなければ
        if (!_arrived)
        {
            //移動数値を最初にリセット
            _velocity = Vector3.zero;
            //着地していれば動かす
            if (_enemyController.isGrounded)
            {
                //移動有無をアニメーションに伝達
                _animator.SetFloat("Speed", 2.0f);
                //目標と自分との座標差を計算し、それを0～1基準にして代入
                _direction = (_target.transform.position - transform.position).normalized;
                //正規化した移動情報に移動速度をかけ、最終的な移動数値を確定
                _velocity = _direction * _moveSpeed;
            }
            //さらに、重力をかける
            _velocity.y += Physics.gravity.y * Time.deltaTime;
            //実際に移動させる
            _enemyController.Move(_velocity * Time.deltaTime);

            //　目的地に到着したかどうかの判定
            if (Vector3.Distance(transform.position, _target.transform.position) < 1.0f)
            {
                //到着していれば判定をオン、アニメータ側に停止信号を送る
                _arrived = true;
                _animator.SetFloat("Speed", 0.0f);
            }

        }
        //対象の傍に到着済みの場合
        else
        {     
            _direction = (_target.transform.position - transform.position).normalized;
            //　対象との距離が離れているかどうかの判定
            if (Vector3.Distance(transform.position, _target.transform.position) >= 1.0f)
            {
                //離れているなら再度追尾開始する
                _arrived = false;
            }


        }

    }

}
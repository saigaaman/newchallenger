﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemySimpleHpGauge : MonoBehaviour
{
#pragma warning disable 0649

    public Image _HpGauge;

    void Start()
    {
     
    }


    private void Update()
    {

    }





    //以下３ステータスを外部からアクセス可能
    //最大ステータス/現在ステータス*100でゲージを出す
    public void HpGaugeUpdate(float maxHp, float hp)
    {
        _HpGauge.fillAmount = (hp / maxHp);
    }







}

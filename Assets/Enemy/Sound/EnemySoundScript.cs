﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemySoundScript : MonoBehaviour
{
#pragma warning disable 0649
#pragma warning disable 0414
    private AudioSource[] _audioSource;

    //アクションのサウンド
    public AudioClip[] _ActionSound;
    //被ダメ時のサウンド
    public AudioClip[] _HitSound;
    //エフェクトサウンド
    public AudioClip[] _EffectSound;

    private void Awake()
    {
        _audioSource = GetComponents<AudioSource>();
    }


    //アクションする時のサウンド
    public void PlayActionSound(string clipName)
    {
        //FirstOrDefaultの場合、例外が発生しなければ規定値（intなら0）となる
        AudioClip selectSE = _ActionSound.FirstOrDefault(n => n.name == clipName);

        _audioSource[1].clip = selectSE;
        _audioSource[1].Play();

    }


    //攻撃命中用サウンド
    public void PlayHitSound(string clipName)
    {
        //FirstOrDefaultの場合、例外が発生しなければ規定値（intなら0）となる
        AudioClip selectSE = _HitSound.FirstOrDefault(n => n.name == clipName);

        _audioSource[2].clip = selectSE;
        _audioSource[2].Play();

    }


    //エフェクトのサウンド）
    public void PlayEffectSound(string clipName)
    {
        //FirstOrDefaultの場合、例外が発生しなければ規定値（intなら0）となる
        AudioClip selectSE = _EffectSound.FirstOrDefault(n => n.name == clipName);

        _audioSource[3].clip = selectSE;
        _audioSource[3].PlayOneShot(selectSE);

    }











}

